<?php

/**
 * Implements hook_menu().
 */
function webform_wcr_menu() {
  $items = array();

  $items['node/%webform_menu/webform/component-revision'] = array(
    'title' => t('Component Revisions'),
    'description' => 'changed-revision',
    'page callback' => '_show_webform_component_revision',
    'page arguments' => array(1),    
    'access callback' => 'webform_node_update_access',
    'access arguments' => array(1),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}


/**
 * Returns component revision markup.
 */
function _show_webform_component_revision($node) {
  $markup = '';
  $nid = $node->nid;
  $query_params = drupal_get_query_parameters();

  $cid = isset($query_params['cid']) && is_numeric($query_params['cid']) 
    ? $query_params['cid'] 
    : FALSE;
  
  $prev_link = '';
  $next_link = '';
  $page = isset($query_params['page']) && is_numeric($query_params['page']) &&  $query_params['page'] > 0 
    ? $query_params['page'] 
    : 0;
  $limit = variable_get('wcr_pagination_limit', 10);
  $offset = $page * $limit;

  $revisions = db_select('webform_component_revision', 'fcr')
    ->fields('fcr')
    ->condition('nid', $nid);

  // Filter with specific component.
  if ($cid) {
    $revisions->condition('cid', $cid);
  }

  $revisions = $revisions
    ->range($offset, $limit)
    ->orderBy('timestamp', 'DESC')
    ->execute()
    ->fetchAll();

  $revision_count = count($revisions);

  if (!empty($revisions)) {
    module_load_include('inc', 'webform', 'includes/webform.components');
    
    // Pagination links.
    if ($page != 0) {
      $prev_link .= '&#171; &#171; ';
      $prev_link .= l(t('Previous'), 'node/' . $nid . '/webform/component-revision', array('query' => array_merge($query_params, array('page' => $page - 1, 'link' => 'prev'))));
    }
    if ($revision_count == $limit) {
      $next_link .= '&nbsp;&nbsp;&nbsp;';
      $next_link .= l(t('Next'), 'node/' . $nid . '/webform/component-revision', array('query' => array_merge($query_params, array('page' => $page + 1, 'link' => 'next'))));
      $next_link .= ' &#187; &#187;';
    }
    
    $pagination = '
    <div class="wcr-pagination" style="float:right;">
      ' . $prev_link  .  $next_link . '
    </div>
    ';

    // Header pagination.
    $markup .= $pagination;

    // Revision markup.
    foreach ($revisions as $revision) {
      $markup .= _get_component_revision_markup($node, $revision);
    }

    // Footer pagination.
    $markup .= $pagination;
  }
  elseif (empty($revisions) && $query_params['link'] == 'next'){
    // Adding previous link to empty result page when landed via 'Next' link. 
    $prev_link .= '&#171; &#171; ';
    $prev_link .= l(t('Previous'), 'node/' . $nid . '/webform/component-revision', array('query' => array_merge($query_params, array('page' => $page - 1, 'link' => 'prev'))));
    
    $pagination = '
    <div class="wcr-pagination" style="float:right;">
      ' . $prev_link . '
    </div>
    ';

    $markup = $pagination . '<div>No revision available.</div>';
  }
  else {
    $markup = '<div>No revision available.</div>';
  }

  // Setting page title overriding various form page titles.
  drupal_set_title("Component Revision");

  return $markup;
}


/**
 * Returns comparitive markup per component.
 */
function _get_component_revision_markup($node, $revision) {
  $nid = $node->nid;
  $cid = $revision->cid;
  $action = $revision->action;

  // Load existing component.
  $existing_component = isset($node->webform['components'][$cid]) 
    ? $node->webform['components'][$cid] 
    : FALSE;
  
  // Design title markup.
  $title_str = '';
  if ($existing_component && !$revision->retained) {
    $name = $existing_component['name'];
    $title_str = _get_revision_component_title_markup($node, $revision, $name);
  }
  else {
    $name = '<span style="color:#aaa;">Retained Component</span>';
    $title_str = _get_revision_component_title_markup($node, $revision, $name);
  }

  $old_state = NULL;
  $new_state = NULL;
  $component_type = NULL;
  if ($revision->old_state) {
    $old_state = unserialize($revision->old_state);
    $component_type = isset($old_state['type']) ? $old_state['type'] : $component_type;
  }
  if ($revision->new_state) {
    $new_state = unserialize($revision->new_state);
    $component_type = isset($new_state['type']) ? $new_state['type'] : $component_type;
  }

  $default_component = _load_default_webform_component($component_type);
  if (!$default_component) {
    $msg = "Component type '$component_type' no longer supported.";
    $markup_data = array(
      'cid' => $cid,
      'action' => $action,
      'header' => $title_str,
      'old_state' => $msg,
      'new_state' => '',
    );
    
    return $markup = _revision_element_markup($markup_data);
  }

  
  if ($action == 'create') {
    $old_state = $default_component;
    $new_state = array_replace_recursive($default_component, $new_state);
  }
  elseif ($action == 'update') {
    $old_state = array_replace_recursive($default_component, $old_state);
    $new_state = array_replace_recursive($default_component, $new_state);
    $new_state['required'] = "" . $new_state['required'];
  }
  elseif ($action == 'delete') {
    $old_state = array_replace_recursive($default_component, $old_state);
    $new_state = $default_component;
  }
  
  // Get state difference.
  $state_diff = _diff_multi_dimensional_array($new_state, $old_state);
  unset($state_diff['nid']);

  $old_state_markup = '[Old State]<br />Nothing to show.';
  if ($old_state !== $default_component) {    
    $old_state_markup = _get_revision_component_form_markup($node, $old_state, $state_diff);
  }

  $new_state_markup = '[New State]<br />Nothing to show.';
  if ($new_state !== $default_component) {
    $new_state_markup = _get_revision_component_form_markup($node, $new_state, $state_diff);
  } 

  $markup_data = array(
    'cid' => $cid,
    'action' => $action,
    'header' => $title_str,
    'old_state' => $old_state_markup,
    'new_state' => $new_state_markup,
  );
  $markup = _revision_element_markup($markup_data);

  return $markup;
}


/**
 * Returns component form markup per logged state old/new.
 */
function _get_revision_component_form_markup($node, $state, $state_diff) {
  $state_form = drupal_get_form('webform_component_edit_form', $node, $state);
  _trim_component_form($state_form, $state_diff);

  // Weight element is not the part of component hence exclusively managed here.
  if (array_key_exists('weight', $state_diff)) {
    _add_form_markup_element($state_form, 'Weight', $state['weight']);
  }

  return render($state_form);
}


/**
 * Returns revision component title markup.
 */
function _get_revision_component_title_markup($node, $revision, $name) {
  $component_link = l(t('[C'. $revision->cid . ']'), 'node/' . $node->nid . '/webform/component-revision', array('query' => array('cid' => $revision->cid)));
  
  $title_str = '<div class="wcr-name"><strong>' . $component_link . ' ' . $name . '</strong></div>';
  $title_str .= '<div class="wcr-detail">' . strtoupper($revision->action . 'd') . " on " . format_date($revision->timestamp) . "</div>";

  return $title_str;
}


/**
 * Returns each row comparing old and new component states.
 */
function _revision_element_markup($data) {
  $markup = 
  '<div class="wcr-element wcr-element-' . $data['cid'] . ' wcr-action-' . $data['action'] . '">'
     .  $data['header'] . 
    '<hr />
    <table class="wcr-table">
      <tr>
        <td class="wcr-old-state" style="width:50%; vertical-align:top; background:#FFFAFA;">' . $data['old_state'] . '</td>
        <td class="wcr-new-state" style="width:50%; vertical-align:top;">' . $data['new_state'] . '</td>
      </tr>
    </table>
  </div>';
  
  return $markup;
}


/**
 * Implements hook_webform_component_insert().
 */
function webform_wcr_webform_component_insert($component) {
  $action = 'create';
  _log_component_revision($component, $action);
}


/**
 * Implements hook_webform_component_update().
 */
function webform_wcr_webform_component_update($component) { 
  $action = 'update';
  _log_component_revision($component, $action);
}


/**
 * Implements hook_webform_component_delete().
 */
function webform_wcr_webform_component_delete($component) {

  // Delete instead of retaining.
  if (!variable_get('wcr_retain_deleted_revisions')) {
    db_delete('webform_component_revision')
      ->condition('nid', $component['nid'])
      ->condition('cid', $component['cid'])
      ->execute();

    return;
  }

  // Retain the component state.
  $action = 'delete';
  _log_component_revision($component, $action, 1);
}


/**
 * Logs component revision.
 */
function _log_component_revision($component, $action, $retained = 0){
  global $user;
  $cid = $component['cid'];
  $nid = $component['nid'];
  $node = node_load($nid);

  $old_state = NULL;
  if ($action != 'create') {
    $old_state = $node->webform['components'][$component['cid']];
  }

  $new_state = NULL;
  if ($action != 'delete') {
    $new_state = $component;
  }

  // Update component retained flag.
  if ($retained) {
    $values = array(
      'retained' => $retained,
    );
    
    db_update('webform_component_revision')
      ->fields($values)
      ->condition('nid', $nid)
      ->condition('cid', $cid)
      ->execute();
  }

  $field_values = array(
      'uid' => $user->uid,
      'nid' => $nid,
      'cid' => $cid,
      'action' => $action,
      'old_state' => serialize($old_state),
      'new_state' => serialize($new_state),
      'retained' => $retained,
      'timestamp' => REQUEST_TIME,
  );
  db_insert('webform_component_revision')
      ->fields($field_values)
      ->execute();
}


/**
 * Admin Settings at admin/config/content/webform.
 */
function webform_wcr_form_webform_admin_settings_alter(&$form, $form_state) {

  // Component revision settings.
  $form['wcr_settings'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Component Revision'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );

  // Retain deleted component states.
  $form['wcr_settings']['wcr_retain_deleted_revisions'] = array(
    '#type' => 'checkbox',
    '#title' => 'Retain deleted component revisions',
    '#default_value' => variable_get('wcr_retain_deleted_revisions', TRUE),
  );

  // Pagination.
  $pagination_limit = array(
    5 => 5,
    10 => 10,
    25 => 25,
    50 => 50,
    75 => 75,
    100 => 100,
  );
  $form['wcr_settings']['wcr_pagination_limit'] = array(
    '#type' => 'select',
    '#title' => t('Pagination Limit'),
    '#options' => $pagination_limit,
    '#default_value' => variable_get('wcr_pagination_limit', 10),
    '#description' => t('Limits number of revisions shown per page.'),
  );
  
}


/**
 * Returs default structure of webform component. 
 */
function _load_default_webform_component($type) {
  module_load_include('inc', 'webform', 'includes/webform.components');

  $components = webform_components();
  $component = in_array($type, array_keys($components)) 
    ? array('type' => $type,) 
    : FALSE;

  if ($component) {
    webform_component_defaults($component);
  }

  return $component;
}

function _add_form_markup_element(&$form, $key, $value) {
  $form['wcr_' . $key] = array(
    '#type' => 'markup',
    '#markup'=> "<div><strong>$key</strong><br />$value</div>",
  );
}


/**
 * Trims form elements and include only elements in $state_diff.
 */
function _trim_component_form(&$form, $state_diff) {
  // $component['extra'] values are used inside form elements $form['display'], $form['validation'] etc.
  // Managing values to include/exclude elements while trimming the $form elements.
  $extra_keys = array();
  $include_elements = array_keys($state_diff);
  if (array_key_exists('required', $state_diff)) {
      $extra_keys[] = 'required';
  }
  if (array_key_exists('extra', $state_diff)) {
    foreach ($state_diff['extra'] as $k => $v) {
      $extra_keys[] = "extra[" . $k . "]";
    }
    $include_elements[] = 'display';
    $include_elements[] = 'validation';
  }

  unset($form['actions']);
  unset($form['#submit']);

  foreach ($form as $k => $v) {
    if (!in_array($k, $include_elements)) {
      unset($form[$k]);
    }
    elseif ($k == 'extra' || $k == 'display' || $k == 'validation') {
      _remove_element_with_name($form, $k, $extra_keys);
    }
  }

}


/**
 * Implements hook_preprocess_webform_components_form().
 * Add 'revision' link per component row.
 */
function webform_wcr_preprocess_webform_components_form(&$variables) {
  $node = $variables['form']['#node'];
  $rows =  $variables['rows'];
  $rows_count = count($rows);

  foreach ($rows as $key => $val) {
    if (isset($val['data-cid'])) {
      $cid = $val['data-cid'];
      $variables['rows'][$key]['data'][] = array(
        'data' => l(t('Revision'), 'node/' . $node->nid . '/webform/component-revision', array('query' => array('cid' => $cid))), 
        'class' => array('webform-component-revision')
      );
    }
  }

  // Update header colspan.
  $header_count = count($variables['header']);
  $variables['header'][$header_count - 1]['colspan'] += 1;

  // Update ADD Form colspan.
  $add_form_elements_count = count($rows[$rows_count - 1]['data']);
  $variables['rows'][$rows_count - 1]['data'][$add_form_elements_count - 1]['colspan'] += 1;
}


/**
 * Implements hook_node_delete().
 * Delete all revisions when node deleted.
 */
function webform_wcr_node_delete($node) {
  if ($node->type == 'webform') {
    db_delete('webform_component_revision')
      ->condition('nid', $node->nid)
      ->execute();
  }
}


/**
 * Removes form element with specific #name.
 */
function _remove_element_with_name(&$form, $element, $extra_keys) {
  $show = FALSE;
  foreach ($form[$element] as $nkey => $nval) {
    if (isset($form[$element][$nkey]['#name'])) {
      if (!in_array($form[$element][$nkey]['#name'], $extra_keys)) {
        unset($form[$element][$nkey]);
      }
      else {
        $show = TRUE;
      }
    }
  }

  // Hide fieldset element if no child element included.
  if (!$show) {
    unset($form[$element]);
  }
}


/**
 * Return multi-dimensional array difference.
 */
function _diff_multi_dimensional_array($arr1, $arr2) {
  $arr_diff = array();

  foreach($arr1 as $key => $val) {
    if (is_array($val) && isset($arr2[$key])) {
      $temp = _diff_multi_dimensional_array($val, $arr2[$key]);
      if ($temp) {
        $arr_diff[$key] = $temp;
      }
    }
    elseif (!isset($arr2[$key])) {
      $arr_diff[$key] = null;
    }
    elseif ($val !== $arr2[$key]) {
      $arr_diff[$key] = $arr2[$key];
    }

    if (isset($arr2[$key])) {
      unset($arr2[$key]);
    }
  }

  return array_merge($arr_diff, $arr2);
}
