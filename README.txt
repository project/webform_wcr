CONTENTS OF THIS FILE
---------------------
- Introduction
- Requirements
- Installation
- Configuration


INTRODUCTION
------------
Webform Component Revision module allows you to maintain 
component revisions per webform node.

While managing components of webform node, this module tracks
create, update and delete action per component.

Maintaing revisions of webform components may help in
finding out any changes made to the component purposefully or accidently.

May also help in regenerating the component if accidently deleted,
by finding the exact component configuration on the component revision page
as it retains the deleted component states as well(configurable).

Check 'CONFIGURATION' section regarding module usage.


REQUIREMENTS
------------
It requires 'Webform' module.
As it works as an extension to the Webform module.

 
INSTALLATION
------------
Install as you would normally install a contributed Drupal module. 
See: https://www.drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------
To configure, find "Configure" link at admin/modules against the module name,
or access directly by visiting admin/config/content/webform.

In order to limit the number of configuration pages,
we have manged to extend the settings of 'Webform' module itself.

Now, to configure at the configuration page find 'COMPONENT REVISION' section.
And you can configure below values which are specific to this module:
(1) Retain deleted component revisions-
    Check it to retain the revisions logged for deleted components as well.
(2) Pagination Limit-
    Limits number of revisions shown per page.

Basically this module supports viewing revisions on following basis:
(1) Per webform component-
    Find 'Revision' link against each component at node/%/webform page.
(2) All webform components-
    Find 'Component Revisions' sub-tab at node/%/webform page.
